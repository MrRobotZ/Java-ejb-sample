package rest;


import entity.Student;
import entity.StudentCourses;

import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import java.util.List;


@Stateless
//@Stateful
public class Repository {

//    private Collection<Student> students;
//
//    private Student student;

    @Inject
    private EntityManager em;

    public List<Student> getAllStudents() {
        return em.createQuery("SELECT s FROM Student s", Student.class).getResultList();
    }

//    public Collection printGradeHour(){
//        Collection<Float> x =  em.createQuery("SELECT SUM(c.grade*c.course.hours) FROM Student s JOIN s.courses c GROUP BY s.id", Float.class).getResultList();
//        return x;
//    }

//    @Transactional
    public List<Student> calculateGPA(){
        List<Student> students = getAllStudents();

        for(Student s : students){

            Float accumlate = 0.0F;
            Float total = 0.0F;

            List<StudentCourses> studentCourses = s.getCourses();
            for (StudentCourses sc: studentCourses){
                Float rand = (float) Math.random() * 4;
                Float hours = (float) sc.getCourse().getHours();
                sc.setGrade(rand);

                accumlate += (rand * hours);
                total += hours;
            }
//            em.merge(studentCourses);

            Float gpa = accumlate / total;
            s.setGpa(gpa);
            System.out.println(gpa);
            em.merge(s);
        }

        return students;
    }


}
