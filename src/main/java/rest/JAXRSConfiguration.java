package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import static constants.Constants.APP_PATH;

/**
 * @author Mawaziny
 */
@ApplicationPath(APP_PATH)
public class JAXRSConfiguration extends Application {

}
