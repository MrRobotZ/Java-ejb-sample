package entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.EmbeddedId;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

import java.util.Objects;

@Entity
@Table(name = "STUDENT_COURSES")
public class StudentCourses {

    @EmbeddedId
    private StudentCoursesId id;

    @Column(name = "GRADES")
    private Float grade;


    @ManyToOne
    @JoinColumn(name = "STUDENT_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @JsonIgnore
    private Student student;

    @ManyToOne
    @JoinColumn(name = "COURSE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private Course course;

//    @MapsId("studentId")
//    @JoinColumn(name = "STUDENT_ID", insertable = false, updatable = false)
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Student student;


//    @MapsId("courseId")
//    @JoinColumn(name = "COURSE_ID", insertable = false, updatable = false)
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Course course;


    public StudentCourses() {
    }

    public StudentCourses(Student student, Course course) {
        this.student = student;
        this.course = course;
        this.id = new StudentCoursesId(student.getId(), course.getId());
    }


    public StudentCoursesId getId() {
        return id;
    }

    public void setId(StudentCoursesId id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Float getGrade() {
        return grade;
    }

    public void setGrade(Float grade) {
        this.grade = grade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass())
            return false;

        StudentCourses that = (StudentCourses) o;
        return Objects.equals(student, that.student) &&
                Objects.equals(course, that.course);
    }

    @Override
    public int hashCode() {
        return Objects.hash(student, course);
    }
}
