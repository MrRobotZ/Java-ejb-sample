package constants;


public class Constants {
    public static final String APP_PATH = "ejb-demo";
    public static final String PERSISTENT_UNIT = "student-context";
    public static final String SCHEMA_NAME = "STUDENTDB";

    private Constants() {
    }
}